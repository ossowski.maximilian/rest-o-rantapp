INSERT INTO public.employee(id, name, pesel, "position", salary, surname, boss_id) VALUES
    (100, 'Marcin', 90022833388, 'Owner', 9000.00, 'Kowalski', 100),
    (101, 'Milosz', 92081113488, 'Chef', 5325.30, 'Adamski', 100),
    (102, 'Ewelina', 95022812348, 'Waiter', 3220.49, 'Burzynska', 101),
    (103, 'Tomek', 93022854321, 'Waiter', 3010.22, 'Czuba', 101),
    (104, 'Malgorzata', 99122866753, 'waiter', 2743.84, 'Porebska', 101);

INSERT INTO public.client(id, name, surname, discount) VALUES
     (200, 'Bogdan', 'Skoczylasz', 0.05),
     (201, 'Milena', 'Smakosz', 0.0),
     (202, 'Hildegarda', 'Maruda', 0.0),
     (203, 'Witold', 'Siemanex', 0.0),
     (204, 'Elzbieta', 'Radziszewska', 0.1);

INSERT INTO public.inventory(id, name, quantity) VALUES
    (50, 'Maka', 100.0),
    (51, 'Mleko', 64.0),
    (52, 'Ser Gouda', 154.5),
    (53, 'Mozzarella', 220.3),
    (54, 'Szynka', 32.75);

INSERT INTO public.dish(id, name, description, price, is_available) VALUES
    (300, 'Pizza Margarita', 'Sos pomidorowy, ser gouda, mozzarella', 18.49, true),
    (301, 'Pizza Prosciutto', 'Sos pomidorowy, ser gouda, mozzarella, szynka', 19.49, true),
    (302, 'Pizza Salame', 'Sos pomidorowy, ser gouda, mozzarella, pikantna salami', 19.99, true),
    (303, 'Pizza Funghi', 'Sos pomidorowy, ser gouda, mozzarella, pieczarki', 19.49, false),
    (304, 'Pizza Roma', 'Sos pomidorowy, ser gouda, mozzarella, szynka, pikantna salami, papryka', 22.49, true),
    (305, 'Pizza Frutti de Mare', 'Sos smietanowy, ser gouda, mozzarella, owoce morza', 25.49, true);

INSERT INTO public.drink(id, name, description, price, volume_ml, is_available) VALUES
    (350, 'Coca Cola', 'Orzezwiajaca, schlodzona cola', 7.49, 200.0, false),
    (351, 'Coca Cola', 'Orzezwiajaca, schlodzona cola', 12.00, 500.0, true),
    (352, 'Woda San Pelegrino', 'Znana wloska woda mineralna - gazowana', 15.99, 1500.0, true),
    (353, 'Woda Panna', 'Znana wloska woda mineralna - niegazowana', 15.99, 1500.0, true),
    (354, 'Wino stolowe czerwone', 'Nasza wino stolowe - kieliszek', 11.49, 150.0, true),
    (355, 'Wino stolowe czerwone', 'Nasza wino stolowe - butelka', 32.66, 750.0, true);

INSERT INTO public.receipt(id, price_with_discount, tip, receipt_date, employee_id, client_id) VALUES
    (400, 58.97, 5.0, '2020-01-20T13:23:54', 102, 201),
    (401, 93.47, 8.0, '2020-01-20T15:20:54', 102, 203),
    (402, 40.48, 3.5, '2020-01-21T16:02:33', 103, 202);

INSERT INTO public.order_dish(id, quantity, dish_id, receipt_id) VALUES
    (501, 1, 300, 400),
    (502, 1, 301, 400),
    (503, 2, 301, 401),
    (504, 1, 304, 401),
    (505, 1, 305, 402);

INSERT INTO public.order_drink(id, quantity, drink_id, receipt_id) VALUES
    (551, 1, 352, 400),
    (552, 2, 351, 401),
    (553, 1, 354, 402);
