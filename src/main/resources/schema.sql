DROP TABLE IF EXISTS dish, drink, employee, client, inventory, receipt, order_dish, order_drink;

CREATE TABLE IF NOT EXISTS dish
(
	id serial NOT NULL,
	name character varying(100) NOT NULL,
	description character varying(255) NOT NULL,
	price double precision NOT NULL,
	is_available boolean NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS drink
(
	id serial NOT NULL,
	name character varying(100) NOT NULL,
	description character varying(255) NOT NULL,
	price double precision NOT NULL,
	volume_ml int NOT NULL,
	is_available boolean NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS employee
(
	id serial NOT NULL,
	pesel bigint NOT NULL,
	name character varying(50) NOT NULL,
	surname character varying(50) NOT NULL,
	position character varying(50) NOT NULL,
	salary double precision NOT NULL,
	boss_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (boss_id) REFERENCES employee(id)
);

CREATE TABLE IF NOT EXISTS client
(
	id serial NOT NULL,
	name character varying(50) NOT NULL,
	surname character varying(50) NOT NULL,
	discount double precision NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS inventory
(
	id serial NOT NULL,
	name character varying(50) NOT NULL,
	quantity double precision NOT NULL
);

CREATE TABLE IF NOT EXISTS receipt
(
	id serial NOT NULL,
	price_with_discount double precision NOT NULL,
	tip double precision NOT NULL,
	receipt_date timestamp NOT NULL,
	employee_id int NOT NULL,
	client_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (employee_id) REFERENCES employee(id),
	FOREIGN KEY (client_id) REFERENCES client(id)
);

CREATE TABLE IF NOT EXISTS order_dish
(
	id serial NOT NULL,
	quantity int NOT NULL,
	dish_id int NOT NULL,
	receipt_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (dish_id) REFERENCES dish(id),
	FOREIGN KEY (receipt_id) REFERENCES receipt(id)
);

CREATE TABLE IF NOT EXISTS order_drink
(
	id serial NOT NULL,
	quantity int NOT NULL,
	drink_id int NOT NULL,
	receipt_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (drink_id) REFERENCES drink(id),
	FOREIGN KEY (receipt_id) REFERENCES receipt(id)
);
