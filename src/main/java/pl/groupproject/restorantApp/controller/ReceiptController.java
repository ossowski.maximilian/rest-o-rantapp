package pl.groupproject.restorantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import pl.groupproject.restorantApp.controller.dto.DishDTO;
import pl.groupproject.restorantApp.controller.dto.DrinkDTO;
import pl.groupproject.restorantApp.controller.dto.ReceiptDTO;
import pl.groupproject.restorantApp.domain.model.Receipt;
import pl.groupproject.restorantApp.domain.service.ReceiptService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/receipt")
public class ReceiptController {

    private final ReceiptService receiptService;

    @Autowired
    public ReceiptController(ReceiptService receiptService) {
        this.receiptService = receiptService;
    }

    @GetMapping(path = "/{id}")
    public ReceiptDTO getReceipt(@PathVariable("id") Long id) {
        return new ReceiptDTO(receiptService.getReceipt(id));
    }

    @GetMapping(path = "/byDate")
    public List<ReceiptDTO> getReceiptByDate(@RequestParam(name = "date")
                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {

        List<Receipt> receiptList = receiptService.getReceiptByDate(date);
        return receiptList.stream()
                .map(ReceiptDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping(path = "/create")
    public Long createReceipt(@RequestParam(name = "employeeId") Long employeeId,
                              @RequestParam(name = "clientId") Long clientId
    ) {
        Receipt receipt = receiptService.createReceipt(
                employeeId,
                clientId
        );
        return receipt.getId();
    }

    @PutMapping(path = "/addDish")
    public DishDTO addDishToReceipt(@RequestParam(name = "dishId") Long dishId,
                                    @RequestParam(name = "receiptId") Long receiptId
    ) {
        return new DishDTO(receiptService.addDish(dishId, receiptId));
    }

    @PutMapping(path = "/addDrink")
    public DrinkDTO addDrinkToReceipt(@RequestParam(name = "drinkId") Long drinkId,
                                     @RequestParam(name = "receiptId") Long receiptId
    ) {
        return new DrinkDTO(receiptService.addDrink(drinkId, receiptId));
    }

    @PutMapping(path = "/change/quantityDish")
    public Integer changeQuantityDish(@RequestParam(name = "dishId") Long dishId,
                                      @RequestParam(name = "receiptId") Long receiptId,
                                      @RequestParam(name = "quantity") Integer quantity
    ) {
        return receiptService.changeQuantityDish(dishId, receiptId, quantity);
    }

    @PutMapping(path = "/change/quantityDrink")
    public Integer changeQuantityDrink(@RequestParam(name = "drinkId") Long drinkId,
                                      @RequestParam(name = "receiptId") Long receiptId,
                                      @RequestParam(name = "quantity") Integer quantity
    ) {
        return receiptService.changeQuantityDrink(drinkId, receiptId, quantity);
    }
    
    @DeleteMapping(path = "/removeDish")
    public void deleteDish(@RequestParam(name = "dishId") Long dishId,
                           @RequestParam(name = "receiptId") Long receiptId
    ) {
        receiptService.removeDish(dishId, receiptId);
    }

    @DeleteMapping(path = "/removeDrink")
    public void deleteDrink(@RequestParam(name = "drinkId") Long drinkId,
                           @RequestParam(name = "receiptId") Long receiptId
    ) {
        receiptService.removeDrink(drinkId, receiptId);
    }

    @DeleteMapping(path = "/remove/{id}")
    public void deleteDish(@PathVariable("id") Long id) {
        receiptService.removeReceipt(id);
    }

}
