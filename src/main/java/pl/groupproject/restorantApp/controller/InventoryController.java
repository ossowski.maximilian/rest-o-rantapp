package pl.groupproject.restorantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import pl.groupproject.restorantApp.controller.dto.InventoryDTO;
import pl.groupproject.restorantApp.domain.model.Inventory;
import pl.groupproject.restorantApp.domain.service.InventoryService;
import pl.groupproject.restorantApp.exception.NotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

    private final InventoryService inventoryService;

    @Autowired
    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping(path = "/{id}")
    public InventoryDTO inventoryDTO(@PathVariable("id") Long id) {

        Optional<Inventory> inventoryOptional = inventoryService.getInventory(id);

        if (!inventoryOptional.isPresent()) {
            throw new NotFoundException();
        }

        return new InventoryDTO(inventoryOptional.get());
    }

    @GetMapping(path = "/all")
    public List<InventoryDTO> getAllInventory(@RequestParam Integer page, @RequestParam Integer size) {
        return inventoryService.getAllInventory(PageRequest.of(page, size))
                .stream()
                .map(InventoryDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping(path = "/add")
    public Long createInventory(@RequestBody InventoryDTO inventoryDTO) {

        return inventoryService.createInventory(
                inventoryDTO.getName(),
                inventoryDTO.getQuantity()
        );

    }

    @PutMapping("/update/{id}")
    public Long updateInventory(@RequestBody InventoryDTO inventoryDTO, @PathVariable("id") Long id) {
        Long inventoryId = inventoryService.updateInventory(
                id,
                inventoryDTO.getName(),
                inventoryDTO.getQuantity()
        );
        return inventoryId;
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeInventory(@PathVariable("id") Long id) {
        inventoryService.removeInventory(id);
    }

}
