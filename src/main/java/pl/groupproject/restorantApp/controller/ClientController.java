package pl.groupproject.restorantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import pl.groupproject.restorantApp.controller.dto.ClientDTO;
import pl.groupproject.restorantApp.domain.model.Client;
import pl.groupproject.restorantApp.domain.service.ClientService;
import pl.groupproject.restorantApp.exception.NotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(path = "/{id}")
    public ClientDTO getClient(@PathVariable("id") Long id) {

        Optional<Client> clientOptional = clientService.getClient(id);

        if (!clientOptional.isPresent()) {
            throw new NotFoundException();
        }

        return new ClientDTO(clientOptional.get());

    }

    @GetMapping(path = "/all")
    public List<ClientDTO> getAllClients(@RequestParam Integer page, @RequestParam Integer size) {
        return clientService.getAllClients(PageRequest.of(page, size))
                .stream()
                .map(ClientDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    @RequestMapping("/add")
    public Long createClient(@RequestBody ClientDTO clientDTO) {
        Long clientId = clientService.createClient(
                clientDTO.getName(),
                clientDTO.getSurname(),
                clientDTO.getDiscount()
        );
        return clientId;
    }

    @PutMapping
    @RequestMapping("/update/{id}")
    public Long updateClient(@RequestBody ClientDTO clientDTO, @PathVariable("id") Long id) {
        Long clientId = clientService.updateClient(
                id,
                clientDTO.getName(),
                clientDTO.getSurname(),
                clientDTO.getDiscount()
        );
        return clientId;
    }

    @DeleteMapping
    @RequestMapping(path = "/remove/{id}")
    public void removeClient(@PathVariable("id") Long id) {
        clientService.removeClient(id);
    }

}
