package pl.groupproject.restorantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import pl.groupproject.restorantApp.controller.dto.EmployeeDTO;
import pl.groupproject.restorantApp.domain.model.Employee;
import pl.groupproject.restorantApp.domain.service.EmployeeService;
import pl.groupproject.restorantApp.exception.NotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(path = "/{id}")
    public EmployeeDTO getEmployee(@PathVariable("id") Long id) {

        Optional<Employee> employeeOptional = employeeService.getEmployee(id);

        if (!employeeOptional.isPresent()) {
            throw new NotFoundException();
        }

        return new EmployeeDTO(employeeOptional.get());

    }

    @GetMapping(path = "/all")
    public List<EmployeeDTO> getAllEmployees(@RequestParam Integer page, @RequestParam Integer size) {
        return employeeService.getAllEmployees(PageRequest.of(page, size))
                .stream()
                .map(EmployeeDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    @RequestMapping("/add")
    public Long createEmployee(@RequestBody EmployeeDTO employeeDTO) {

        return employeeService.createEmployee(
                employeeDTO.getPesel(),
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getPosition(),
                employeeDTO.getSalary(),
                employeeDTO.getBossId()
        );

    }

    @PutMapping("/update")
    public Long updateEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.updateEmployee(
                employeeDTO.getId(),
                employeeDTO.getPesel(),
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getPosition(),
                employeeDTO.getSalary(),
                employeeDTO.getBossId()
        );
    }


    @PutMapping
    @RequestMapping("/update/{id}")
    public Long updateEmployee(@RequestBody EmployeeDTO employeeDTO, @PathVariable Long id) {
        return employeeService.updateEmployee(
                id,
                employeeDTO.getPesel(),
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getPosition(),
                employeeDTO.getSalary(),
                employeeDTO.getBossId()
        );
    }

    @DeleteMapping
    @RequestMapping(path = "/remove/{id}")
    public void removeEmployee(@PathVariable("id") Long id) {
        employeeService.removeEmployee(id);
    }

}
