package pl.groupproject.restorantApp.controller.dto;

import pl.groupproject.restorantApp.domain.model.Inventory;

import java.io.Serializable;
import java.math.BigDecimal;

public class InventoryDTO implements Serializable {

    private Long id;

    private String name;

    private BigDecimal quantity;

    public InventoryDTO() {
    }

    public InventoryDTO(Inventory inventory) {
        this.id = inventory.getId();
        this.name = inventory.getName();
        this.quantity = inventory.getQuantity();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
