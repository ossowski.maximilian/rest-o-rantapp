package pl.groupproject.restorantApp.controller.dto;

import pl.groupproject.restorantApp.domain.model.Drink;

import java.math.BigDecimal;

public class DrinkDTO {

    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Integer volumeMl;

    private Boolean isAvailable;

    public DrinkDTO() {
    }

    public DrinkDTO(Drink drink) {
        this.id = drink.getId();
        this.name = drink.getName();
        this.description = drink.getDescription();
        this.price = drink.getPrice();
        this.volumeMl = drink.getVolumeMl();
        this.isAvailable = drink.getIsAvailable();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getVolumeMl() {
        return volumeMl;
    }

    public void setVolumeMl(Integer volumeMl) {
        this.volumeMl = volumeMl;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean available) {
        isAvailable = available;
    }

    public Drink toDomain() {
        return new Drink(
                id,
                name,
                description,
                price,
                volumeMl,
                isAvailable
        );
    }
}
