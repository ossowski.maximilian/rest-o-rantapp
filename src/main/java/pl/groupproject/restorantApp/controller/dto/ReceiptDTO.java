package pl.groupproject.restorantApp.controller.dto;

import pl.groupproject.restorantApp.domain.model.OrderDish;
import pl.groupproject.restorantApp.domain.model.OrderDrink;
import pl.groupproject.restorantApp.domain.model.Receipt;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ReceiptDTO {

    private Long id;

    private BigDecimal priceWithDiscount;

    private BigDecimal tip;

    private LocalDateTime receiptDate;

    private EmployeeDTO employee;

    private ClientDTO client;


    private List<DishListDTO> dishes;
    private List<DrinkListDTO> drinks;

    public ReceiptDTO(Receipt receipt) {
        this.id = receipt.getId();
        this.priceWithDiscount = receipt.getPriceWithDiscount()
                .setScale(2, RoundingMode.CEILING);
        this.tip = receipt.getTip();
        this.receiptDate = receipt.getReceiptDate();
        this.employee = new EmployeeDTO(receipt.getEmployee());
        this.client = new ClientDTO(receipt.getClient());

        List<DishListDTO> dishList = new ArrayList<>();
        for (OrderDish orderDish : receipt.getDishes()) {
            dishList.add(
                    new DishListDTO(
                            new DishDTO(orderDish.getDish()),
                            orderDish.getQuantity()));
        }
        this.dishes = dishList;

        List<DrinkListDTO> drinkList = new ArrayList<>();
        for (OrderDrink orderDrink : receipt.getDrinks()) {
            drinkList.add(
                    new DrinkListDTO(
                            new DrinkDTO(orderDrink.getDrink()),
                            orderDrink.getQuantity()));
        }
        this.drinks = drinkList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public LocalDateTime getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(LocalDateTime receiptDate) {
        this.receiptDate = receiptDate;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public List<DishListDTO> getDishes() {
        if(this.dishes == null){
            this.dishes = new ArrayList<>();
        }
        return dishes;
    }

    public void setDishes(List<DishListDTO> dishes) {
        this.dishes = dishes;
    }

    public List<DrinkListDTO> getDrinks() {
        if(this.drinks == null){
            this.drinks = new ArrayList<>();
        }
        return drinks;
    }

    public void setDrinks(List<DrinkListDTO> drinks) {
        this.drinks = drinks;
    }

}
