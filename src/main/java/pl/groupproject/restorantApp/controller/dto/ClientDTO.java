package pl.groupproject.restorantApp.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.groupproject.restorantApp.domain.model.Client;
import pl.groupproject.restorantApp.domain.model.Receipt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ClientDTO implements Serializable {

    private Long id;

    private String name;

    private String surname;

    private BigDecimal discount;

    @JsonIgnore
    private List<Receipt> receipts;

    public ClientDTO() {
    }

    public ClientDTO(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.surname = client.getSurname();
        this.discount = client.getDiscount();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public List<Receipt> getReceipts() {
        return receipts;
    }

    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }
}
