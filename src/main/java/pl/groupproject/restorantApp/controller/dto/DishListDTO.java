package pl.groupproject.restorantApp.controller.dto;

public class DishListDTO {

    private DishDTO dish;

    private Integer quantity;

    public DishListDTO(DishDTO dish, Integer quantity) {
        this.dish = dish;
        this.quantity = quantity;
    }

    public DishDTO getDish() {
        return dish;
    }

    public void setDish(DishDTO dish) {
        this.dish = dish;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
