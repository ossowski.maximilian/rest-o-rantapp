package pl.groupproject.restorantApp.controller.dto;

import pl.groupproject.restorantApp.domain.model.Dish;

import java.math.BigDecimal;

public class DishDTO {

    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Boolean isAvailable;

    public DishDTO() {
    }

    public DishDTO(Dish dish) {
        this.id = dish.getId();
        this.name = dish.getName();
        this.description = dish.getDescription();
        this.price = dish.getPrice();
        this.isAvailable = dish.getAvailable();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean available) {
        isAvailable = available;
    }

    public Dish toDomain() {
        return new Dish(
                id,
                name,
                description,
                price,
                isAvailable
        );
    }
}
