package pl.groupproject.restorantApp.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.groupproject.restorantApp.domain.model.Employee;
import pl.groupproject.restorantApp.domain.model.Receipt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class EmployeeDTO implements Serializable {

    private Long id;

    private Long pesel;

    private String name;

    private String surname;

    private String position;

    private BigDecimal salary;

    private Long bossId;
    @JsonIgnore
    private List<Receipt> receipts;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Employee employee) {
        this.id = employee.getId();
        this.pesel = employee.getPesel();
        this.name = employee.getName();
        this.surname = employee.getSurname();
        this.position = employee.getPosition();
        this.salary = employee.getSalary();
        this.bossId = employee.getBoss().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Long getBossId() {
        return bossId;
    }

    public void setBossId(Long bossId) {
        this.bossId = bossId;
    }

    public List<Receipt> getReceipts() {
        return receipts;
    }

    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }
}
