package pl.groupproject.restorantApp.controller.dto;

public class DrinkListDTO {
    
        private DrinkDTO drink;

        private Integer quantity;

        public DrinkListDTO(DrinkDTO drink, Integer quantity) {
            this.drink = drink;
            this.quantity = quantity;
        }

        public DrinkDTO getDrink() {
            return drink;
        }

        public void setDrink(DrinkDTO drink) {
            this.drink = drink;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }
    

}
