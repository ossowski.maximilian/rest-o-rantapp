package pl.groupproject.restorantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import pl.groupproject.restorantApp.controller.dto.DishDTO;
import pl.groupproject.restorantApp.domain.service.DishService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/dish")
public class DishController {

    private final DishService dishService;

    @Autowired
    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping(path = "/{id}")
    public DishDTO getDish(@PathVariable("id") Long id) {
        return new DishDTO(dishService.getDish(id));
    }

    @GetMapping(path = "/getAll")
    public List<DishDTO> getAllDish(@RequestParam(name = "page") Integer page,
                                    @RequestParam(name = "elements") Integer elements) {
        return dishService.getAllDish(PageRequest.of(page, elements))
                .stream()
                .map(DishDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping(path = "/add")
    public Long createDish(@RequestBody DishDTO dishDTO) {
        return dishService.createDish(
                dishDTO.getName(),
                dishDTO.getDescription(),
                dishDTO.getPrice(),
                dishDTO.getIsAvailable()
        );
    }

    @PutMapping(path = "/update/{id}")
    public DishDTO updateDish(@RequestBody DishDTO dishDTO, @PathVariable Long id) {
        return new DishDTO(
                dishService.updateDish(
                        id,
                        dishDTO.getName(),
                        dishDTO.getDescription(),
                        dishDTO.getPrice(),
                        dishDTO.getIsAvailable()
                )
        );
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeDish(@PathVariable("id") Long id) {
        dishService.removeDish(id);
    }
}



