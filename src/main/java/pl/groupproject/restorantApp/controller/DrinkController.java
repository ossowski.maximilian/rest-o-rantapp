package pl.groupproject.restorantApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import pl.groupproject.restorantApp.controller.dto.DrinkDTO;
import pl.groupproject.restorantApp.domain.service.DrinkService;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/drink")
public class DrinkController {

    private final DrinkService drinkService;

    @Autowired
    public DrinkController(DrinkService drinkService) {
        this.drinkService = drinkService;
    }

    @GetMapping(path = "/{id}")
    public DrinkDTO getDrink(@PathVariable("id") Long id) {
        return new DrinkDTO(drinkService.getDrink(id));
    }

    @GetMapping(path = "/getAll")
    public List<DrinkDTO> getAllDrink(@RequestParam(name = "page") Integer page,
                                      @RequestParam(name = "elements") Integer elements) {
        return drinkService.getAllDrink(PageRequest.of(page, elements))
                .stream()
                .map(DrinkDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping(path = "/add")
    public Long createDrink(@RequestBody DrinkDTO drinkDTO) {
        return drinkService.createDrink(
                drinkDTO.getName(),
                drinkDTO.getDescription(),
                drinkDTO.getPrice(),
                drinkDTO.getVolumeMl(),
                drinkDTO.getIsAvailable()
        );
    }

    @PutMapping(path = "/update/{id}")
    public DrinkDTO updateDrink(@RequestBody DrinkDTO drinkDTO, @PathVariable Long id) {
        return new DrinkDTO(
                drinkService.updateDrink(
                        id,
                        drinkDTO.getName(),
                        drinkDTO.getDescription(),
                        drinkDTO.getPrice(),
                        drinkDTO.getVolumeMl(),
                        drinkDTO.getIsAvailable()
                )
        );
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeDrink(@PathVariable("id") Long id) {
        drinkService.removeDrink(id);
    }
}
