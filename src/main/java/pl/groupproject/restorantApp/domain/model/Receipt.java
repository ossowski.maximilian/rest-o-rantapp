package pl.groupproject.restorantApp.domain.model;

import pl.groupproject.restorantApp.infrastructure.entity.ReceiptHibernate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Receipt extends AbstractModel {

    private Long id;

    private BigDecimal priceWithDiscount;

    private BigDecimal tip;

    private LocalDateTime receiptDate;

    private Employee employee;

    private Client client;

    private List<OrderDish> dishes;

    private List<OrderDrink> drinks;

    public Receipt(Long id,
                   BigDecimal priceWithDiscount,
                   BigDecimal tip,
                   LocalDateTime receiptDate,
                   Client client,
                   Employee employee,
                   List<OrderDish> orderDishList,
                   List<OrderDrink> orderDrinkList
                   ) {
        this.id = id;
        this.priceWithDiscount = priceWithDiscount;
        this.tip = tip;
        this.receiptDate = receiptDate;
        this.client = client;
        this.employee = employee;
        this.dishes = orderDishList;
        this.drinks = orderDrinkList;
    }

    public Receipt(Long id,
                   BigDecimal priceWithDiscount,
                   BigDecimal tip,
                   LocalDateTime receiptDate,
                   Client client,
                   Employee employee
    ) {
        this.id = id;
        this.priceWithDiscount = priceWithDiscount;
        this.tip = tip;
        this.receiptDate = receiptDate;
        this.client = client;
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public LocalDateTime getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(LocalDateTime receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<OrderDish> getDishes() {
        if(this.dishes == null){
            this.dishes = new ArrayList<>();
        }
        return dishes;
    }

    public void setDishes(List<OrderDish> dishes) {
        this.dishes = dishes;
    }

    public List<OrderDrink> getDrinks() {
        if(this.drinks == null){
            this.drinks = new ArrayList<>();
        }
        return drinks;
    }

    public void setDrinks(List<OrderDrink> drinks) {
        this.drinks = drinks;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ReceiptHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("priceWithDiscount=" + priceWithDiscount)
                .add("tip=" + tip)
                .add("receiptDate=" + receiptDate)
                .toString();
    }
}
