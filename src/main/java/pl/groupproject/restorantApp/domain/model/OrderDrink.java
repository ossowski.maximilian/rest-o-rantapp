package pl.groupproject.restorantApp.domain.model;

import java.util.StringJoiner;

public class OrderDrink extends AbstractModel {

    private Long id;

    private Integer quantity;

    private Drink drink;

    private Receipt receipt;

    public OrderDrink(
            Long id,
            Integer quantity,
            Drink drink,
            Receipt receipt) {
        this.id = id;
        this.quantity = quantity;
        this.drink = drink;
        this.receipt = receipt;
    }

    public OrderDrink(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OrderDrink.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("quantity=" + quantity)
                .add("drink=" + drink)
                .add("receipt=" + receipt)
                .toString();
    }
}
