package pl.groupproject.restorantApp.domain.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Employee extends AbstractModel {

    private Long id;

    private Long pesel;

    private String name;

    private String surname;

    private String position;

    private BigDecimal salary;

    private Employee boss;

    private Long bossId;

    private List<Receipt> receipts;

    public Employee(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary
            ) {
        this.id = id;
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.salary = salary;
    }

    public Employee(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Employee boss) {
        this.id = id;
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.salary = salary;
        this.boss = boss;
    }

    public Employee(
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId) {
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.salary = salary;
        this.bossId = bossId;
    }

    public Long getId() {
        return id;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public Long getBossId() {
        return bossId;
    }

    public void setBossId(Long bossId) {
        this.bossId = bossId;
    }

    public List<Receipt> getReceipts() {
        if (this.receipts == null) {
            this.receipts = new ArrayList<>();
        }
        return receipts;
    }

    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Employee.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("pesel=" + pesel)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("position='" + position + "'")
                .add("salary=" + salary)
                .toString();
    }
}
