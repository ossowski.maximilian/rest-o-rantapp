package pl.groupproject.restorantApp.domain.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;

public class Drink extends AbstractModel {

    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Integer volumeMl;

    private Boolean isAvailable;

    private Set<OrderDrink> orderDrinks;

    public Drink(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.volumeMl = volumeMl;
        this.isAvailable = isAvailable;
    }

    public Drink(
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.volumeMl = volumeMl;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getVolumeMl() {
        return volumeMl;
    }

    public void setVolumeMl(Integer volumeMl) {
        this.volumeMl = volumeMl;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean available) {
        isAvailable = available;
    }

    public Set<OrderDrink> getOrderDrinks() {
        if (this.orderDrinks == null) {
            this.orderDrinks = new HashSet<>();
        }
        return orderDrinks;
    }

    public void setReceipts(Set<OrderDrink> orderDrinks) {
        this.orderDrinks = orderDrinks;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Drink.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("volumeMl=" + volumeMl)
                .add("isAvailable=" + isAvailable)
                .toString();
    }
}
