package pl.groupproject.restorantApp.domain.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Client extends AbstractModel {

    private Long id;

    private String name;

    private String surname;

    private BigDecimal discount;

    private List<Receipt> receipts;

    public Client(
            Long id,
            String name,
            String surname,
            BigDecimal discount) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.discount = discount;
    }

    public Client(
            String name,
            String surname,
            BigDecimal discount) {
        this.name = name;
        this.surname = surname;
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public List<Receipt> getReceipts() {
        if (this.receipts == null) {
            receipts = new ArrayList<>();
        }
        return receipts;
    }

    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Client.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("discount=" + discount)
                .toString();
    }

}
