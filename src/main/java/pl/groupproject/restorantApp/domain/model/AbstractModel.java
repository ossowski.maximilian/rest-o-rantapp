package pl.groupproject.restorantApp.domain.model;

import java.util.Objects;

public abstract class AbstractModel {

    protected Long id;

    public AbstractModel() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractModel that = (AbstractModel) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
