package pl.groupproject.restorantApp.domain.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Dish extends AbstractModel {

    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Boolean isAvailable;

    private List<OrderDish> orderDish;

    public Dish(Long id, String name, String description, BigDecimal price, Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public List<OrderDish> getOrderDish() {
        if(this.orderDish == null){
            this.orderDish = new ArrayList<>();
        }
        return orderDish;
    }

    public void setOrderDish(List<OrderDish> orderDish) {
        this.orderDish = orderDish;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Dish.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("isAvailable=" + isAvailable)
                .toString();
    }
}
