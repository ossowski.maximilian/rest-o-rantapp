package pl.groupproject.restorantApp.domain.model;

import java.math.BigDecimal;
import java.util.StringJoiner;

public class Inventory extends AbstractModel {

    private Long id;

    private String name;

    private BigDecimal quantity;

    public Inventory(
            Long id,
            String name,
            BigDecimal quantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    public Inventory(
            String name,
            BigDecimal quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Inventory.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("quantity=" + quantity)
                .toString();
    }

}
