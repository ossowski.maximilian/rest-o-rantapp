package pl.groupproject.restorantApp.domain.model;

import java.util.StringJoiner;

public class OrderDish extends AbstractModel {

    private Long id;

    private Integer quantity;

    private Receipt receipt;

    private Dish dish;

    public OrderDish() {
    }

    public OrderDish(Long id,
                     Integer quantity,
                     Dish dish,
                     Receipt receipt
    ) {
        this.id = id;
        this.quantity = quantity;
        this.dish = dish;
        this.receipt = receipt;
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OrderDish.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("quantity=" + quantity)
                .add("receipt=" + receipt)
                .add("dish=" + dish)
                .toString();
    }

}
