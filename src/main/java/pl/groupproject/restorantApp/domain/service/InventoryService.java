package pl.groupproject.restorantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Inventory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface InventoryService {

    Long createInventory(
            String name,
            BigDecimal quantity);

    Long updateInventory(
            Long id,
            String name,
            BigDecimal quantity);

    Optional<Inventory> getInventory(Long id);

    List<Inventory> getAllInventory(Pageable pageable);

    void removeInventory(Long id);
}
