package pl.groupproject.restorantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.groupproject.restorantApp.domain.model.Dish;
import pl.groupproject.restorantApp.domain.repository.DishRepository;
import pl.groupproject.restorantApp.domain.service.DishService;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.exception.ObjectExistsException;
import pl.groupproject.restorantApp.exception.ObjectExistsOnReceipt;
import pl.groupproject.restorantApp.infrastructure.repository.OrderDishHibernateRepository;

import java.math.BigDecimal;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {

    private final DishRepository dishRepository;
    private final OrderDishHibernateRepository orderDishHibernateRepository;

    @Autowired
    public DishServiceImpl(DishRepository dishRepository,
                           OrderDishHibernateRepository orderDishHibernateRepository) {
        this.dishRepository = dishRepository;
        this.orderDishHibernateRepository = orderDishHibernateRepository;
    }

    @Override
    public Long createDish(String name,
                           String description,
                           BigDecimal price,
                           Boolean isAvailable) {


        if (dishRepository.getByName(name).isPresent()) {
            throw new ObjectExistsException();
        }

        Dish dish = dishRepository.createDish(
                name,
                description,
                price,
                isAvailable
        );
        return dish.getId();
    }

    @Override
    public Dish getDish(Long id) {
        return dishRepository.getDish(id)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public Dish getByName(String name){
        return dishRepository.getByName(name)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public List<Dish> getAllDish(Pageable pageable) {
        return dishRepository.getAllDish(pageable);
    }

    @Override
    public Dish updateDish(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Boolean isAvailable) {

        return dishRepository.updateDish(
                id,
                name,
                description,
                price,
                isAvailable
        ).orElseThrow(NotFoundException::new);

    }

    @Override
    public void removeDish(Long id) {
        if (!dishRepository.getDish(id).isPresent()) {
            throw new NotFoundException();
        }

        if (orderDishHibernateRepository.findByDish_Id(id).isPresent()) {
            throw new ObjectExistsOnReceipt();
        }

        dishRepository.removeDish(id);
        orderDishHibernateRepository.deleteById(id);
    }
}
