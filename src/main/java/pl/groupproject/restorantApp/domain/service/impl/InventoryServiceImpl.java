package pl.groupproject.restorantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.groupproject.restorantApp.domain.model.Inventory;
import pl.groupproject.restorantApp.domain.repository.InventoryRepository;
import pl.groupproject.restorantApp.domain.service.InventoryService;
import pl.groupproject.restorantApp.exception.NotFoundException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class InventoryServiceImpl implements InventoryService {

    private final InventoryRepository inventoryRepository;

    @Autowired
    public InventoryServiceImpl(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    @Override
    public Long createInventory(
            String name,
            BigDecimal quantity) {

        Inventory inventory = inventoryRepository.createInventory(
                name,
                quantity);

        return inventory.getId();
    }

    public Long updateInventory(
            Long id,
            String name,
            BigDecimal quantity) {

        Optional<Inventory> inventoryOptional = inventoryRepository.getInventory(id);
        if (!inventoryOptional.isPresent()){
            throw new NotFoundException();
        }

        inventoryRepository.updateInventory(id, name, quantity);

        return id;

    }


    @Override
    public Optional<Inventory> getInventory(Long id) {
        return inventoryRepository.getInventory(id);
    }

    @Override
    public List<Inventory> getAllInventory(Pageable pageable) {
        return inventoryRepository.getAllInventory(pageable);
    }

    @Override
    public void removeInventory(Long id) {
        inventoryRepository.removeInventory(id);
    }
}
