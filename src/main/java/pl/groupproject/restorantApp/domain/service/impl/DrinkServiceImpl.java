package pl.groupproject.restorantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.groupproject.restorantApp.domain.model.Drink;
import pl.groupproject.restorantApp.domain.repository.DrinkRepository;
import pl.groupproject.restorantApp.domain.repository.OrderDrinkRepository;
import pl.groupproject.restorantApp.domain.service.DrinkService;
import pl.groupproject.restorantApp.exception.NotAvailableException;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.infrastructure.entity.OrderDrinkHibernate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class DrinkServiceImpl implements DrinkService {

    private final DrinkRepository drinkRepository;
    private final OrderDrinkRepository orderDrinkRepository;

    @Autowired
    public DrinkServiceImpl(DrinkRepository drinkRepository,
                            OrderDrinkRepository orderDrinkRepository) {
        this.drinkRepository = drinkRepository;
        this.orderDrinkRepository = orderDrinkRepository;
    }

    @Override
    public Long createDrink(String name,
                            String description,
                            BigDecimal price,
                            Integer volumeMl,
                            Boolean isAvailable) {

        Drink drink = drinkRepository.createDrink(
                name,
                description,
                price,
                volumeMl,
                isAvailable);

        return drink.getId();

    }

    @Override
    public Drink getDrink(Long id) {
        return drinkRepository.getDrink(id)
                .orElseThrow(NotFoundException::new);
        }

    @Override
    public List<Drink> getAllDrink(Pageable pageable) {
        return drinkRepository.getAllDrink(pageable);
    }

    @Override
    public Drink updateDrink(Long id,
                             String name,
                             String description,
                             BigDecimal price,
                             Integer volumeMl,
                             Boolean isAvailable) {
        Optional<Drink> drink = drinkRepository.updateDrink(
                id,
                name,
                description,
                price,
                volumeMl,
                isAvailable
        );
        if (!drink.isPresent()) {
            throw new NotFoundException();
        }
        return drink.get();
    }

    @Override
    public void removeDrink(Long id) {
        for (OrderDrinkHibernate hibernate : orderDrinkRepository.getAllOrderDrink()) {
            Long comparedId = hibernate.getDrink().getId();
            if (comparedId.equals(id)) {
                throw new NotAvailableException();
            }
        }

        if (!drinkRepository.getDrink(id).isPresent()) {
            throw new NotFoundException();
        }

        drinkRepository.removeDrink(id);
    }
}
