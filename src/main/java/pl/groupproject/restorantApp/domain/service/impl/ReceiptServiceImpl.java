package pl.groupproject.restorantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.groupproject.restorantApp.domain.model.*;
import pl.groupproject.restorantApp.domain.repository.*;
import pl.groupproject.restorantApp.domain.service.ReceiptService;
import pl.groupproject.restorantApp.exception.NotAvailableException;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.exception.ObjectExistsException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReceiptServiceImpl implements ReceiptService {

    private final ReceiptRepository receiptRepository;
    private final ClientRepository clientRepository;
    private final EmployeeRepository employeeRepository;
    private final DishRepository dishRepository;
    private final DrinkRepository drinkRepository;

    private final BigDecimal BIG_DECIMAL_ZERO = new BigDecimal(0);

    @Autowired
    public ReceiptServiceImpl(ReceiptRepository receiptRepository,
                              ClientRepository clientRepository,
                              EmployeeRepository employeeRepository,
                              DishRepository dishRepository,
                              DrinkRepository drinkRepository) {
        this.receiptRepository = receiptRepository;
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
        this.dishRepository = dishRepository;
        this.drinkRepository = drinkRepository;
    }

    @Override
    public Receipt createReceipt(
            Long employeeId,
            Long clientId) {

        Client client = clientRepository.getClient(clientId)
                .orElseThrow(NotFoundException::new);

        Employee employee = employeeRepository.getEmployeeWithBoss(employeeId)
                .orElseThrow(NotFoundException::new);

        return receiptRepository.createReceipt(
                BIG_DECIMAL_ZERO,
                BIG_DECIMAL_ZERO,
                LocalDateTime.now(),
                client,
                employee
        );
    }

    @Override
    public Dish addDish(Long dishId, Long receiptId) {

        Dish dish = dishRepository.getDish(dishId)
                .orElseThrow(NotFoundException::new);
        Receipt receipt = receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);

        if (!dish.getAvailable()) {
            throw new NotAvailableException();
        }

        List<OrderDish> orderDish = receipt.getDishes();
        List<Long> list = orderDish
                .stream()
                .map(OrderDish::getDish)
                .map(Dish::getId)
                .collect(Collectors.toList());

        if (list.contains(dishId)) {
            throw new ObjectExistsException();
        }

        BigDecimal newDishPrice = checkForClientDiscount(receipt, dish);

        BigDecimal priceWithDiscount = receipt.getPriceWithDiscount()
                .add(newDishPrice);

        Dish newDish = receiptRepository.addDish(dish, receipt);

        updateReceipt(
                receiptId,
                priceWithDiscount,
                receipt.getTip(),
                receipt.getReceiptDate()
        );

        return newDish;
    }

    @Override
    public Drink addDrink(Long drinkId, Long receiptId) {

        Drink drink = drinkRepository.getDrink(drinkId)
                .orElseThrow(NotFoundException::new);
        Receipt receipt = receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);

        if (!drink.getIsAvailable()) {
            throw new NotAvailableException();
        }

        List<OrderDrink> orderDrink = receipt.getDrinks();
        List<Long> list = orderDrink
                .stream()
                .map(OrderDrink::getDrink)
                .map(Drink::getId)
                .collect(Collectors.toList());

        if (list.contains(drinkId)) {
            throw new ObjectExistsException();
        }

        BigDecimal newDrinkPrice = checkForClientDiscount(receipt, drink);

        BigDecimal priceWithDiscount = receipt.getPriceWithDiscount()
                .add(newDrinkPrice);

        Drink newDrink = receiptRepository.addDrink(drink, receipt);

        updateReceipt(
                receiptId,
                priceWithDiscount,
                receipt.getTip(),
                receipt.getReceiptDate()
        );

        return newDrink;
    }

    @Override
    public Integer changeQuantityDish(Long dishId, Long receiptId, Integer quantity) {

        Receipt receipt = receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);

        OrderDish orderDishTooChange = null;

        for(OrderDish orderDish : receipt.getDishes()){
            if(orderDish.getDish().getId().equals(dishId)){
                orderDishTooChange = orderDish;
            }
        }

        if(orderDishTooChange == null){
            throw new NotFoundException();
        }

        Integer oldQuantity = orderDishTooChange.getQuantity();
        Dish dish = orderDishTooChange.getDish();
        BigDecimal priceWithDiscount = receipt.getPriceWithDiscount();
        int differenceCheck = oldQuantity + quantity;
        Integer newQuantity = 0;

        if (differenceCheck < 0) {
            throw new NotAvailableException();
        }

        if (differenceCheck > 0) {

            newQuantity = receiptRepository.changeQuantityOfDish(dishId, receiptId, quantity);

            int quantityDifference = newQuantity - oldQuantity;
            BigDecimal multiplicand = new BigDecimal(quantityDifference);

            BigDecimal newDishPrice = checkForClientDiscount(receipt, dish);

            priceWithDiscount = priceWithDiscount.add(newDishPrice.multiply(multiplicand));

            updateReceipt(
                    receiptId,
                    priceWithDiscount,
                    receipt.getTip(),
                    receipt.getReceiptDate()
            );
        }

        if (differenceCheck == 0) {
            removeDish(dishId, receiptId);
            newQuantity = 0;
        }
        return newQuantity;
    }

    @Override
    public Integer changeQuantityDrink(Long drinkId, Long receiptId, Integer quantity) {

        Receipt receipt = receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);

        OrderDrink orderDrinkTooChange = null;

        for(OrderDrink orderDrink : receipt.getDrinks()){
            if(orderDrink.getDrink().getId().equals(drinkId)){
                orderDrinkTooChange = orderDrink;
            }
        }

        if(orderDrinkTooChange == null){
            throw new NotFoundException();
        }

        Integer oldQuantity = orderDrinkTooChange.getQuantity();
        Drink drink = orderDrinkTooChange.getDrink();
        BigDecimal priceWithDiscount = receipt.getPriceWithDiscount();
        int differenceCheck = oldQuantity + quantity;
        Integer newQuantity = 0;

        if (differenceCheck < 0) {
            throw new NotAvailableException();
        }

        if (differenceCheck > 0) {

            newQuantity = receiptRepository.changeQuantityOfDrink(drinkId, receiptId, quantity);

            int quantityDifference = newQuantity - oldQuantity;
            BigDecimal multiplicand = new BigDecimal(quantityDifference);

            BigDecimal newDrinkPrice = checkForClientDiscount(receipt, drink);

            priceWithDiscount = priceWithDiscount.add(newDrinkPrice.multiply(multiplicand));

            updateReceipt(
                    receiptId,
                    priceWithDiscount,
                    receipt.getTip(),
                    receipt.getReceiptDate()
            );
        }

        if (differenceCheck == 0) {
            removeDrink(drinkId, receiptId);
            newQuantity = 0;
        }
        return newQuantity;
    }

    @Override
    public Receipt updateReceipt(Long id,
                                 BigDecimal priceWithDiscount,
                                 BigDecimal tip,
                                 LocalDateTime receiptDate
    ) {
        Receipt receipt = receiptRepository.getReceipt(id)
                .orElseThrow(NotFoundException::new);

        return receiptRepository.updateReceipt(
                receipt,
                priceWithDiscount,
                tip,
                receiptDate
        );
    }

    @Override
    public Receipt getReceipt(Long receiptId) {
        return receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);
    }


    @Override
    public List<Receipt> getReceiptByDate(LocalDate date) {
        return receiptRepository.getReceiptByDate(date);
    }

    @Override
    public void removeDish(Long dishId, Long receiptId) {
        Receipt receipt = receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);

        OrderDish orderDishTooChange = null;

        for(OrderDish orderDish : receipt.getDishes()){
            if(orderDish.getDish().getId().equals(dishId)){
                orderDishTooChange = orderDish;
            }
        }

        if(orderDishTooChange == null){
            throw new NotFoundException();
        }

        Dish dish = orderDishTooChange.getDish();
        BigDecimal oldQuantity = new BigDecimal(orderDishTooChange.getQuantity());

        BigDecimal priceWithDiscount = receipt.getPriceWithDiscount();

        receiptRepository.removeDish(dishId, receiptId);

        BigDecimal newDishPrice = checkForClientDiscount(receipt, dish);
        priceWithDiscount = priceWithDiscount.subtract(newDishPrice.multiply(oldQuantity));

        updateReceipt(
                receiptId,
                priceWithDiscount,
                receipt.getTip(),
                receipt.getReceiptDate()
        );
    }

    @Override
    public void removeDrink(Long drinkId, Long receiptId) {
        Receipt receipt = receiptRepository.getReceipt(receiptId)
                .orElseThrow(NotFoundException::new);

        OrderDrink orderDrinkTooChange = null;

        for(OrderDrink orderDrink : receipt.getDrinks()){
            if(orderDrink.getDrink().getId().equals(drinkId)){
                orderDrinkTooChange = orderDrink;
            }
        }

        if(orderDrinkTooChange == null){
            throw new NotFoundException();
        }

        Drink drink = orderDrinkTooChange.getDrink();
        BigDecimal oldQuantity = new BigDecimal(orderDrinkTooChange.getQuantity());

        BigDecimal priceWithDiscount = receipt.getPriceWithDiscount();

        receiptRepository.removeDrink(drinkId, receiptId);

        BigDecimal newDrinkPrice = checkForClientDiscount(receipt, drink);
        priceWithDiscount = priceWithDiscount.subtract(newDrinkPrice.multiply(oldQuantity));

        updateReceipt(
                receiptId,
                priceWithDiscount,
                receipt.getTip(),
                receipt.getReceiptDate()
        );
    }

    @Override
    public void removeReceipt(Long receiptId) {
        receiptRepository.removeReceipt(receiptId);
    }

    private BigDecimal checkForClientDiscount(Receipt receipt, Dish dish) {
        BigDecimal clientDiscount = receipt.getClient().getDiscount();
        BigDecimal newDishPrice = dish.getPrice();
        if (clientDiscount.compareTo(BIG_DECIMAL_ZERO) > 0) {
            newDishPrice = newDishPrice.subtract(newDishPrice.multiply(clientDiscount));
            return newDishPrice;
        }
        return newDishPrice;
    }

    private BigDecimal checkForClientDiscount(Receipt receipt, Drink drink) {
        BigDecimal clientDiscount = receipt.getClient().getDiscount();
        BigDecimal newDishPrice = drink.getPrice();
        if (clientDiscount.compareTo(BIG_DECIMAL_ZERO) > 0) {
            newDishPrice = newDishPrice.subtract(newDishPrice.multiply(clientDiscount));
            return newDishPrice;
        }
        return newDishPrice;
    }
}
