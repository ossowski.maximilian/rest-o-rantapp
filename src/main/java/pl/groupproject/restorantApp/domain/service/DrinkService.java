package pl.groupproject.restorantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Drink;

import java.math.BigDecimal;
import java.util.List;

public interface DrinkService {

    Long createDrink(
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable
    );

    Drink getDrink(Long id);

    List<Drink> getAllDrink(Pageable pageable);

    Drink updateDrink(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable
    );

    void removeDrink(Long id);

}
