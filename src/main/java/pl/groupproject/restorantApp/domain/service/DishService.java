package pl.groupproject.restorantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Dish;

import java.math.BigDecimal;
import java.util.List;

public interface DishService {

    Long createDish(
            String name,
            String description,
            BigDecimal price,
            Boolean isAvailable
    );

    Dish getDish(Long id);

    Dish getByName(String name);

    List<Dish> getAllDish(Pageable pageable);

    Dish updateDish(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Boolean isAvailable
    );

    void removeDish(Long id);
}
