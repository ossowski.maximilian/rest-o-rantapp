package pl.groupproject.restorantApp.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.groupproject.restorantApp.domain.model.Employee;
import pl.groupproject.restorantApp.domain.repository.EmployeeRepository;
import pl.groupproject.restorantApp.domain.service.EmployeeService;
import pl.groupproject.restorantApp.exception.NotFoundException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Long createEmployee(
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId) {

        Employee boss = employeeRepository.getEmployee(bossId)
                .orElseThrow(NotFoundException::new);

        Employee employee = employeeRepository.createEmployee(
                pesel,
                name,
                surname,
                position,
                salary,
                boss);

        return employee.getId();

    }

    @Override
    public Long updateEmployee(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId) {

        Optional<Employee> employeeOptional = employeeRepository.getEmployee(id);
        if (!employeeOptional.isPresent()){
            throw new NotFoundException();
        }

        employeeRepository.updateEmployee(id, pesel, name, surname, position, salary, bossId);

        return id;

    }

    @Override
    public Optional<Employee> getEmployee(Long id) {
        return employeeRepository.getEmployee(id);
    }

    @Override
    public List<Employee> getAllEmployees(Pageable pageable) {
        return employeeRepository.getAllEmployees(pageable);
    }

    @Override
    public void removeEmployee(Long id) {
        employeeRepository.removeEmployee(id);
    }
}
