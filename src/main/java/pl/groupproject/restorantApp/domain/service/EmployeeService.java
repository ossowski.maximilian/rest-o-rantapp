package pl.groupproject.restorantApp.domain.service;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Employee;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Long createEmployee(
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId);

    Long updateEmployee(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId);

    Optional<Employee> getEmployee(Long id);

    List<Employee> getAllEmployees(Pageable pageable);

    void removeEmployee(Long id);

}
