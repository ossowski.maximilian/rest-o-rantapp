package pl.groupproject.restorantApp.domain.service;

import pl.groupproject.restorantApp.domain.model.Dish;
import pl.groupproject.restorantApp.domain.model.Drink;
import pl.groupproject.restorantApp.domain.model.Receipt;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface ReceiptService {

    Receipt createReceipt(
            Long employeeId,
            Long clientId
    );

    Dish addDish(Long dishId, Long ReceiptId);

    Drink addDrink(Long drinkId, Long ReceiptId);

    Integer changeQuantityDish(Long dishId, Long ReceiptId, Integer quantity);

    Integer changeQuantityDrink(Long drinkId, Long ReceiptId, Integer quantity);

    Receipt updateReceipt(
            Long id,
            BigDecimal priceWithDiscount,
            BigDecimal tip,
            LocalDateTime receiptDate
    );

    Receipt getReceipt(Long receiptId);

    List<Receipt> getReceiptByDate(LocalDate date);

    void removeDish(Long dishId, Long ReceiptId);

    void removeDrink(Long drinkId, Long ReceiptId);

    void removeReceipt(Long ReceiptId);


}
