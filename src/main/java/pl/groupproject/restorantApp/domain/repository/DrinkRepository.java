package pl.groupproject.restorantApp.domain.repository;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Drink;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface DrinkRepository {

    Drink createDrink(
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable
    );

    Optional<Drink> getDrink(Long id);

    List<Drink> getAllDrink(Pageable pageable);

    Optional<Drink> updateDrink(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable
    );

    void removeDrink(Long id);
}
