package pl.groupproject.restorantApp.domain.repository;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Employee;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    Employee createEmployee(
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Employee boss);

    Employee updateEmployee(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId);


    Optional<Employee> getEmployee(Long id);

    Optional<Employee> getEmployeeWithBoss(Long id);

    List<Employee> getAllEmployees(Pageable pageable);

    void removeEmployee(Long id);

}
