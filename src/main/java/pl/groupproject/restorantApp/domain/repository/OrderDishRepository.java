package pl.groupproject.restorantApp.domain.repository;

import pl.groupproject.restorantApp.infrastructure.entity.OrderDishHibernate;

public interface OrderDishRepository {

    Iterable<OrderDishHibernate> getAllOrderDish();
}
