package pl.groupproject.restorantApp.domain.repository;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Inventory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface InventoryRepository {

    Inventory createInventory(
            String name,
            BigDecimal quantity);

    Inventory updateInventory(
            Long id,
            String name,
            BigDecimal quantity);





    Optional<Inventory> getInventory(Long id);

    List<Inventory> getAllInventory(Pageable pageable);

    void removeInventory(Long id);

}
