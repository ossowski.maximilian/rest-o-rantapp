package pl.groupproject.restorantApp.domain.repository;

import pl.groupproject.restorantApp.domain.model.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReceiptRepository {

    Receipt createReceipt(
            BigDecimal priceWithDiscount,
            BigDecimal tip,
            LocalDateTime receiptDate,
            Client client,
            Employee employee
    );

    Dish addDish(Dish dish, Receipt receipt);

    Drink addDrink(Drink drink, Receipt receipt);

    Integer changeQuantityOfDish(Long dishId, Long receiptId, Integer quantity);

    Integer changeQuantityOfDrink(Long drinkId, Long receiptId, Integer quantity);

    Receipt updateReceipt(
            Receipt updatedReceipt,
            BigDecimal priceWithDiscount,
            BigDecimal tip,
            LocalDateTime receiptDate
    );

    Optional<Receipt> getReceipt(Long receiptId);

    List<Receipt> getReceiptByDate(LocalDate date);

    void removeDish(Long dishId, Long receiptId);

    void removeDrink(Long drinkId, Long receiptId);

    void removeReceipt(Long receiptId);

}
