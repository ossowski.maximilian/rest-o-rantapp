package pl.groupproject.restorantApp.domain.repository;

import org.springframework.data.domain.Pageable;
import pl.groupproject.restorantApp.domain.model.Dish;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface DishRepository {

    Dish createDish(
            String name,
            String description,
            BigDecimal price,
            Boolean isAvailable
    );

    Optional<Dish> getDish(Long id);

    Optional<Dish> getByName(String name);

    List<Dish> getAllDish(Pageable pageable);

    Optional<Dish> updateDish(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Boolean isAvailable
    );

    void removeDish(Long id);
}
