package pl.groupproject.restorantApp.domain.repository;

import pl.groupproject.restorantApp.infrastructure.entity.OrderDrinkHibernate;

public interface OrderDrinkRepository {

    Iterable<OrderDrinkHibernate> getAllOrderDrink();
}
