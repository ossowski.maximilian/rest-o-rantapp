package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.InventoryHibernate;

import java.util.Optional;

public interface InventoryHibernateRepository extends PagingAndSortingRepository<InventoryHibernate, Long> {

    Optional<InventoryHibernate> findByName(String name);       //TODO name musi byc unique

}
