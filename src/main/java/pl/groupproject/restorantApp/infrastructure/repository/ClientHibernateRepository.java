package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.ClientHibernate;

public interface ClientHibernateRepository extends PagingAndSortingRepository<ClientHibernate, Long> {

    Page<ClientHibernate> findBySurname(String surname, Pageable pageable);

}
