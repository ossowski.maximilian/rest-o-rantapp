package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.OrderDrinkHibernate;
import pl.groupproject.restorantApp.infrastructure.entity.ReceiptHibernate;

import java.util.List;

public interface OrderDrinkHibernateRepository extends PagingAndSortingRepository<OrderDrinkHibernate, Long> {

    List<OrderDrinkHibernate> findAllByReceipt(ReceiptHibernate receiptHibernate);

    @Modifying
    @Query(value = "DELETE FROM order_drink u WHERE u.id= ?1",
            nativeQuery = true)
    void deleteByIdWithQuery(Long id);

}
