package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.EmployeeHibernate;

import java.util.Optional;

public interface EmployeeHibernateRepository extends PagingAndSortingRepository<EmployeeHibernate, Long> {

    Optional<EmployeeHibernate> findByPesel(Integer pesel);

    Page<EmployeeHibernate> findBySurname(String surname, Pageable pageable);

    Page<EmployeeHibernate> findByPosition(String position, Pageable pageable);

}
