package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.DrinkHibernate;

public interface DrinkHibernateRepository extends PagingAndSortingRepository<DrinkHibernate, Long> {
}
