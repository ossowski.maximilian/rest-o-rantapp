package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.OrderDishHibernate;
import pl.groupproject.restorantApp.infrastructure.entity.ReceiptHibernate;

import java.util.List;
import java.util.Optional;

public interface OrderDishHibernateRepository extends PagingAndSortingRepository<OrderDishHibernate, Long> {

    List<OrderDishHibernate> findAllByReceipt(ReceiptHibernate receiptHibernate);

    @Modifying
    @Query(value = "DELETE FROM order_dish u WHERE u.id= ?1",
            nativeQuery = true)
    void deleteByIdWithQuery(Long id);

    Optional<OrderDishHibernate> findByDish_Id(Long id);


}
