package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.ReceiptHibernate;

import java.util.List;

public interface ReceiptHibernateRepository extends PagingAndSortingRepository<ReceiptHibernate, Long> {

    @Query(value = "SELECT * FROM receipt WHERE receipt_date BETWEEN CAST (?1 AS TIMESTAMP) AND CAST (?2 AS TIMESTAMP)",
    nativeQuery = true)
    List<ReceiptHibernate> findByReceiptDateBetween(String date_start, String date_end);
}
