package pl.groupproject.restorantApp.infrastructure.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.groupproject.restorantApp.infrastructure.entity.DishHibernate;

import java.util.Optional;

public interface DishHibernateRepository extends PagingAndSortingRepository<DishHibernate, Long> {
    Optional<DishHibernate> findByName(String name);
}
