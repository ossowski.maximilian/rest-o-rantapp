package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.Employee;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "employee")
public class EmployeeHibernate extends AbstractEntityHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long pesel;

    private String name;

    private String surname;

    private String position;

    private BigDecimal salary;

    @ManyToOne
    @JoinColumn(name = "boss_id")
    private EmployeeHibernate boss;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ReceiptHibernate> receipts;

    public EmployeeHibernate() {
    }

    public EmployeeHibernate(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary) {
        this.id = id;
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.salary = salary;
    }

    public EmployeeHibernate(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            EmployeeHibernate boss) {
        this.id = id;
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.salary = salary;
        this.boss = boss;
    }

    public EmployeeHibernate(Employee employee) {
        this.id = employee.getId();
        this.pesel = employee.getPesel();
        this.name = employee.getName();
        this.surname = employee.getSurname();
        this.position = employee.getPosition();
        this.salary = employee.getSalary();
        this.boss = new EmployeeHibernate(
                employee.getBoss().getId(),
                employee.getBoss().getPesel(),
                employee.getBoss().getName(),
                employee.getBoss().getSurname(),
                employee.getBoss().getPosition(),
                employee.getBoss().getSalary()
        );
    }

    public Long getId() {
        return id;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public EmployeeHibernate getBoss() {
        return boss;
    }

    public void setBoss(EmployeeHibernate boss) {
        this.boss = boss;
    }

    public List<ReceiptHibernate> getReceipts() {
        if (this.receipts == null) {
            receipts = new ArrayList<>();
        }
        return receipts;
    }

    public void setReceipts(List<ReceiptHibernate> receipts) {
        this.receipts = receipts;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EmployeeHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("pesel=" + pesel)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("position='" + position + "'")
                .add("salary=" + salary)
                .toString();
    }
}
