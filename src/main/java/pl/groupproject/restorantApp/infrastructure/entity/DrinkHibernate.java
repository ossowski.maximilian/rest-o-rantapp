package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.Drink;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "drink")
public class DrinkHibernate extends AbstractEntityHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Integer volumeMl;

    private Boolean isAvailable;

    @OneToMany(mappedBy = "drink", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderDrinkHibernate> orderDrink;

    public DrinkHibernate() {
    }

    public DrinkHibernate(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Integer volumeMl,
            Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.volumeMl = volumeMl;
        this.isAvailable = isAvailable;
    }

    public DrinkHibernate(Drink drink) {
        this.id = drink.getId();
        this.name = drink.getName();
        this.description = drink.getDescription();
        this.price = drink.getPrice();
        this.volumeMl = drink.getVolumeMl();
        this.isAvailable = drink.getIsAvailable();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getVolumeMl() {
        return volumeMl;
    }

    public void setVolumeMl(Integer volumeMl) {
        this.volumeMl = volumeMl;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public List<OrderDrinkHibernate> getOrderDrink() {
        if(this.orderDrink == null){
            this.orderDrink = new ArrayList<>();
        }
        return orderDrink;
    }

    public void setOrderDrink(List<OrderDrinkHibernate> orderDrink) {
        this.orderDrink = orderDrink;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DrinkHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("volumeMl=" + volumeMl)
                .add("isAvailable=" + isAvailable)
                .toString();
    }
}
