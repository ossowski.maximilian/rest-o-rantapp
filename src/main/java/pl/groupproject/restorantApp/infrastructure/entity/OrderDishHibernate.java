package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.OrderDish;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "order_dish")
public class OrderDishHibernate extends AbstractEntityHibernate{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private ReceiptHibernate receipt;

    @ManyToOne
    @JoinColumn(name = "dish_id")
    private DishHibernate dish;

    public OrderDishHibernate() {
    }

    public OrderDishHibernate(Long id,
                              Integer quantity,
                              DishHibernate dishHibernate,
                              ReceiptHibernate receiptHibernate
                              ) {
        this.id = id;
        this.quantity = quantity;
        this.receipt = receiptHibernate;
        this.dish = dishHibernate;
    }

    public OrderDishHibernate(OrderDish orderDish) {
        this.id = orderDish.getId();
        this.quantity = orderDish.getQuantity();
        this.receipt = new ReceiptHibernate(orderDish.getReceipt());
        this.dish = new DishHibernate(orderDish.getDish());
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ReceiptHibernate getReceipt() {
        return receipt;
    }

    public void setReceipt(ReceiptHibernate receipt) {
        this.receipt = receipt;
    }

    public DishHibernate getDish() {
        return dish;
    }

    public void setDish(DishHibernate dish) {
        this.dish = dish;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OrderDishHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("quantity=" + quantity)
                .toString();
    }
}
