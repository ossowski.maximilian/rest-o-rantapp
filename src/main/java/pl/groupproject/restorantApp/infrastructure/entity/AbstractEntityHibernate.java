package pl.groupproject.restorantApp.infrastructure.entity;

import java.util.Objects;

public abstract class AbstractEntityHibernate {

    protected Long id;

    public AbstractEntityHibernate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractEntityHibernate that = (AbstractEntityHibernate) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
