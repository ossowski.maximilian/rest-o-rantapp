package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.Dish;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "dish")
public class DishHibernate extends AbstractEntityHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Boolean isAvailable;

    @OneToMany(mappedBy = "dish", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<OrderDishHibernate> orderDish;

    public DishHibernate() {
    }

    public DishHibernate(Long id, String name, String description, BigDecimal price, Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public DishHibernate(String name, String description, BigDecimal price, Boolean isAvailable) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public DishHibernate(Long id) {
        this.id = id;
    }

    public DishHibernate(Dish dish) {
        this.id = dish.getId();
        this.name = dish.getName();
        this.description = dish.getDescription();
        this.price = dish.getPrice();
        this.isAvailable = dish.getAvailable();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public List<OrderDishHibernate> getOrderDish() {
        if(this.orderDish == null){
            this.orderDish = new ArrayList<>();
        }
        return orderDish;
    }

    public void setOrderDish(List<OrderDishHibernate> orderDishHibernate) {
        this.orderDish = orderDishHibernate;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DishHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("isAvailable=" + isAvailable)
                .toString();
    }

    public Dish toDomain(){
        return new Dish(
                this.getId(),
                this.getName(),
                this.getDescription(),
                this.getPrice(),
                this.getAvailable()
        );
    }

}
