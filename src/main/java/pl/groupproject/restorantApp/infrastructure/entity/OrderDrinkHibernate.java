package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.OrderDrink;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "order_drink")
public class OrderDrinkHibernate extends AbstractEntityHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "drink_id")
    private DrinkHibernate drink;

    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private ReceiptHibernate receipt;

    public OrderDrinkHibernate() {
    }

    public OrderDrinkHibernate(
            Long id,
            Integer quantity,
            DrinkHibernate drinkHibernate,
            ReceiptHibernate receiptHibernate) {
        this.id = id;
        this.quantity = quantity;
        this.drink = drinkHibernate;
        this.receipt = receiptHibernate;
    }

    public OrderDrinkHibernate(OrderDrink orderDrink) {
        this.id = orderDrink.getId();
        this.quantity = orderDrink.getQuantity();
        this.receipt = new ReceiptHibernate(orderDrink.getReceipt());
        this.drink = new DrinkHibernate(orderDrink.getDrink());
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public DrinkHibernate getDrink() {
        return drink;
    }

    public void setDrink(DrinkHibernate drink) {
        this.drink = drink;
    }

    public ReceiptHibernate getReceipt() {
        return receipt;
    }

    public void setReceipt(ReceiptHibernate receipt) {
        this.receipt = receipt;
    }



    @Override
    public String toString() {
        return new StringJoiner(", ", OrderDrinkHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("quantity=" + quantity)
                .add("drinkHibernate=" + drink)
                .add("receiptHibernate=" + receipt)
                .toString();
    }
}
