package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.Receipt;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Entity
@Table(name = "receipt")
public class ReceiptHibernate extends AbstractEntityHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal priceWithDiscount;

    private BigDecimal tip;

    private LocalDateTime receiptDate;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private EmployeeHibernate employee;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientHibernate client;

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<OrderDishHibernate> orderDishes;

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<OrderDrinkHibernate> orderDrinks;

    public ReceiptHibernate() {
    }

    public ReceiptHibernate(Long id){
        this.id = id;
    }

    public ReceiptHibernate(Long id,
                            BigDecimal priceWithDiscount,
                            BigDecimal tip,
                            LocalDateTime receiptDate,
                            ClientHibernate client,
                            EmployeeHibernate employee
    ) {
        this.id = id;
        this.priceWithDiscount = priceWithDiscount;
        this.tip = tip;
        this.receiptDate = receiptDate;
        this.client = client;
        this.employee = employee;
    }

    public ReceiptHibernate(Long id,
                            BigDecimal priceWithDiscount,
                            BigDecimal tip,
                            LocalDateTime receiptDate,
                            EmployeeHibernate employee,
                            ClientHibernate client,
                            List<OrderDishHibernate> orderDishes,
                            List<OrderDrinkHibernate> orderDrinks) {
        this.id = id;
        this.priceWithDiscount = priceWithDiscount;
        this.tip = tip;
        this.receiptDate = receiptDate;
        this.employee = employee;
        this.client = client;
        this.orderDishes = orderDishes;
        this.orderDrinks = orderDrinks;
    }

    public ReceiptHibernate(Receipt receipt) {
        this.id = receipt.getId();
        this.priceWithDiscount = receipt.getPriceWithDiscount();
        this.tip = receipt.getTip();
        this.receiptDate = receipt.getReceiptDate();
        this.employee = new EmployeeHibernate(receipt.getEmployee());
        this.client = new ClientHibernate(receipt.getClient());
        this.orderDishes = receipt.getDishes()
                .stream()
                .map(OrderDishHibernate::new)
                .collect(Collectors.toList());
        this.orderDrinks = receipt.getDrinks()
                .stream()
                .map(OrderDrinkHibernate::new)
                .collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public LocalDateTime getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(LocalDateTime receiptDate) {
        this.receiptDate = receiptDate;
    }

    public EmployeeHibernate getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeHibernate employee) {
        this.employee = employee;
    }

    public ClientHibernate getClient() {
        return client;
    }

    public void setClient(ClientHibernate client) {
        this.client = client;
    }

    public List<OrderDishHibernate> getOrderDishes() {
        if(this.orderDishes == null){
            this.orderDishes = new ArrayList<>();
        }
        return orderDishes;
    }

    public void setOrderDishes(List<OrderDishHibernate> orderDishes) {
        this.orderDishes = orderDishes;
    }

    public List<OrderDrinkHibernate> getOrderDrinks() {
        if (this.orderDrinks == null) {
            this.orderDrinks = new ArrayList<>();
        }
        return orderDrinks;
    }

    public void setDrinks(List<OrderDrinkHibernate> orderDrinks) {
        this.orderDrinks = orderDrinks;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ReceiptHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("priceWithDiscount=" + priceWithDiscount)
                .add("tip=" + tip)
                .add("receiptDate=" + receiptDate)
                .toString();
    }
}
