package pl.groupproject.restorantApp.infrastructure.entity;

import pl.groupproject.restorantApp.domain.model.Client;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "client")
public class ClientHibernate extends AbstractEntityHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String surname;

    private BigDecimal discount;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ReceiptHibernate> receipts;

    public ClientHibernate() {
    }

    public ClientHibernate(
            Long id,
            String name,
            String surname,
            BigDecimal discount) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.discount = discount;
    }

    public ClientHibernate(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.surname = client.getSurname();
        this.discount = client.getDiscount();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public List<ReceiptHibernate> getReceipts() {
        if (this.receipts == null) {
            receipts = new ArrayList<>();
        }
        return receipts;
    }

    public void setReceipts(List<ReceiptHibernate> receipts) {
        this.receipts = receipts;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ClientHibernate.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("discount=" + discount)
                .toString();
    }

}
