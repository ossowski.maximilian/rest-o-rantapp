package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.groupproject.restorantApp.domain.model.*;
import pl.groupproject.restorantApp.domain.repository.ReceiptRepository;
import pl.groupproject.restorantApp.infrastructure.entity.*;
import pl.groupproject.restorantApp.infrastructure.repository.OrderDishHibernateRepository;
import pl.groupproject.restorantApp.infrastructure.repository.OrderDrinkHibernateRepository;
import pl.groupproject.restorantApp.infrastructure.repository.ReceiptHibernateRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ReceiptRepositoryImpl implements ReceiptRepository {

    private final ReceiptHibernateRepository receiptHibernateRepository;
    private final OrderDishHibernateRepository orderDishHibernateRepository;
    private final OrderDrinkHibernateRepository orderDrinkHibernateRepository;

    private final ClientRepositoryImpl clientRepository;
    private final EmployeeRepositoryImpl employeeRepository;
    private final OrderDishRepositoryImpl orderDishRepository;
    private final OrderDrinkRepositoryImpl orderDrinkRepository;


    @Autowired
    public ReceiptRepositoryImpl(ReceiptHibernateRepository receiptHibernateRepository,
                                 OrderDishHibernateRepository orderDishHibernateRepository,
                                 OrderDrinkHibernateRepository orderDrinkHibernateRepository,

                                 ClientRepositoryImpl clientRepository,
                                 EmployeeRepositoryImpl employeeRepository,
                                 OrderDrinkRepositoryImpl orderDrinkRepository,
                                 OrderDishRepositoryImpl orderDishRepository
    ) {

        this.receiptHibernateRepository = receiptHibernateRepository;
        this.orderDishHibernateRepository = orderDishHibernateRepository;
        this.orderDrinkHibernateRepository = orderDrinkHibernateRepository;

        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
        this.orderDishRepository = orderDishRepository;
        this.orderDrinkRepository = orderDrinkRepository;

    }

    @Override
    public Receipt createReceipt(BigDecimal priceWithDiscount,
                                 BigDecimal tip,
                                 LocalDateTime receiptDate,
                                 Client client,
                                 Employee employee) {

        ReceiptHibernate receiptHibernate = new ReceiptHibernate(
                null,
                priceWithDiscount,
                tip,
                receiptDate,
                new ClientHibernate(client),
                new EmployeeHibernate(employee)
        );
        receiptHibernateRepository.save(receiptHibernate);
        return toDomain(receiptHibernate);
    }

    @Override
    public Dish addDish(Dish dish, Receipt receipt) {

        ReceiptHibernate receiptHibernate = new ReceiptHibernate(receipt);

        OrderDishHibernate orderDishHibernate = new OrderDishHibernate(
                null,
                1,
                new DishHibernate(dish),
                receiptHibernate
        );

        List<OrderDishHibernate> orderDishes = receiptHibernate.getOrderDishes();
        orderDishes.add(orderDishHibernate);

        receiptHibernateRepository.save(receiptHibernate);
        return dish;
    }

    @Override
    @Transactional
    public Drink addDrink(Drink drink, Receipt receipt) {

        ReceiptHibernate receiptHibernate = new ReceiptHibernate(receipt);

        OrderDrinkHibernate orderDrinkHibernate = new OrderDrinkHibernate(
                null,
                1,
                new DrinkHibernate(drink),
                receiptHibernate
        );

        List<OrderDrinkHibernate> orderDrinks = receiptHibernate.getOrderDrinks();
        orderDrinks.add(orderDrinkHibernate);

        receiptHibernateRepository.save(receiptHibernate);
        return drink;
    }

    @Override
    public Integer changeQuantityOfDish(Long dishId, Long receiptId, Integer quantity) {
        ReceiptHibernate receiptHibernate = new ReceiptHibernate(receiptId);
        List<OrderDishHibernate> orderDishHibernateList = orderDishHibernateRepository.findAllByReceipt(receiptHibernate);
        OrderDishHibernate orderDishHibernate = new OrderDishHibernate();

        for (OrderDishHibernate orderDishHibernateIter : orderDishHibernateList) {

            if (orderDishHibernateIter
                    .getDish()
                    .getId()
                    .equals(dishId)) {
                orderDishHibernate = orderDishHibernateIter;
            }
        }

        int newQuantity = orderDishHibernate.getQuantity() + quantity;

        orderDishHibernate.setQuantity(newQuantity);

        orderDishHibernateRepository.save(orderDishHibernate);

        return newQuantity;
    }

    @Override
    public Integer changeQuantityOfDrink(Long drinkId, Long receiptId, Integer quantity) {
        ReceiptHibernate receiptHibernate = new ReceiptHibernate(receiptId);
        List<OrderDrinkHibernate> orderDrinkHibernateList = orderDrinkHibernateRepository.findAllByReceipt(receiptHibernate);
        OrderDrinkHibernate orderDrinkHibernate = new OrderDrinkHibernate();

        for (OrderDrinkHibernate orderDrinkHibernateIter : orderDrinkHibernateList) {

            if (orderDrinkHibernateIter
                    .getDrink()
                    .getId()
                    .equals(drinkId)) {
                orderDrinkHibernate = orderDrinkHibernateIter;
            }
        }

        int newQuantity = orderDrinkHibernate.getQuantity() + quantity;

        orderDrinkHibernate.setQuantity(newQuantity);

        orderDrinkHibernateRepository.save(orderDrinkHibernate);

        return newQuantity;
    }

    @Override
    @Transactional
    public Receipt updateReceipt(Receipt updatedReceipt,
                                           BigDecimal priceWithDiscount,
                                           BigDecimal tip,
                                           LocalDateTime receiptDate
    ) {

        ReceiptHibernate updatedReceiptHibernate = new ReceiptHibernate(updatedReceipt);

        updatedReceiptHibernate.setPriceWithDiscount(priceWithDiscount);
        updatedReceiptHibernate.setTip(tip);
        updatedReceiptHibernate.setReceiptDate(receiptDate);

        receiptHibernateRepository.save(updatedReceiptHibernate);

        return toDomain(updatedReceiptHibernate);
    }

    @Override
    public Optional<Receipt> getReceipt(Long receiptId) {

        Optional<ReceiptHibernate> receiptHibernate = receiptHibernateRepository.findById(receiptId);

        return receiptHibernate.map(this::toDomain);
    }


    @Override
    public List<Receipt> getReceiptByDate(LocalDate date) {

        String startDate = date.toString();
        String endDate = date.toString() + " 23:59:59";
        return receiptHibernateRepository.findByReceiptDateBetween(startDate, endDate)
                .stream()
                .map(this::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void removeDish(Long dishId, Long receiptId) {
        ReceiptHibernate hibernate = new ReceiptHibernate(receiptId);
        List<OrderDishHibernate> orderDishHibernateList = orderDishHibernateRepository.findAllByReceipt(hibernate);
        OrderDishHibernate orderDishHibernate = new OrderDishHibernate();

        for (OrderDishHibernate orderDishHibernateIter : orderDishHibernateList) {

            if (orderDishHibernateIter
                    .getDish()
                    .getId()
                    .equals(dishId)) {
                orderDishHibernate = orderDishHibernateIter;
            }
        }
        orderDishHibernateRepository.deleteByIdWithQuery(orderDishHibernate.getId());
    }

    @Override
    @Transactional
    public void removeDrink(Long drinkId, Long receiptId) {
        ReceiptHibernate hibernate = new ReceiptHibernate(receiptId);
        List<OrderDrinkHibernate> orderDrinkHibernateList = orderDrinkHibernateRepository.findAllByReceipt(hibernate);
        OrderDrinkHibernate orderDrinkHibernate = new OrderDrinkHibernate();

        for (OrderDrinkHibernate orderDrinkHibernateIter : orderDrinkHibernateList) {

            if (orderDrinkHibernateIter
                    .getDrink()
                    .getId()
                    .equals(drinkId)) {
                orderDrinkHibernate = orderDrinkHibernateIter;
            }
        }
        orderDrinkHibernateRepository.deleteByIdWithQuery(orderDrinkHibernate.getId());
    }

    @Override
    public void removeReceipt(Long receiptId) {
        receiptHibernateRepository.deleteById(receiptId);
    }

    public Receipt toDomain(ReceiptHibernate hibernate) {
        return new Receipt(
                hibernate.getId(),
                hibernate.getPriceWithDiscount(),
                hibernate.getTip(),
                hibernate.getReceiptDate(),
                clientRepository.toDomain(hibernate.getClient()),
                employeeRepository.toDomainWithBoss(hibernate.getEmployee()),
                hibernate.getOrderDishes()
                        .stream()
                        .map(orderDishRepository::toDomain)
                        .collect(Collectors.toList()),
                hibernate.getOrderDrinks()
                        .stream()
                        .map(orderDrinkRepository::toDomain)
                        .collect(Collectors.toList())
        );
    }




}
