package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Dish;
import pl.groupproject.restorantApp.domain.repository.DishRepository;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.infrastructure.entity.DishHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.DishHibernateRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DishRepositoryImpl implements DishRepository {

    private final DishHibernateRepository dishHibernateRepository;

    @Autowired
    public DishRepositoryImpl(DishHibernateRepository dishHibernateRepository) {
        this.dishHibernateRepository = dishHibernateRepository;
    }

    @Override
    public Dish createDish(String name,
                           String description,
                           BigDecimal price,
                           Boolean isAvailable) {
        DishHibernate dishHibernate = new DishHibernate(
                null,
                name,
                description,
                price,
                isAvailable
        );
        dishHibernateRepository.save(dishHibernate);
        return toDomain(dishHibernate);
    }

    @Override
    public Optional<Dish> getDish(Long id) {
       return dishHibernateRepository.findById(id)
                .map(this::toDomain);
    }

    @Override
    public Optional<Dish> getByName(String name){
        return dishHibernateRepository.findByName(name)
                .map(this::toDomain);

    }

    @Override
    public List<Dish> getAllDish(Pageable pageable) {
        return dishHibernateRepository.findAll(pageable)
                .getContent()
                .stream()
                .map(this::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Dish> updateDish(
            Long id,
            String name,
            String description,
            BigDecimal price,
            Boolean isAvailable
    ) {

        Optional<DishHibernate> dishHibernate = dishHibernateRepository.findById(id);
        if(!dishHibernate.isPresent()) {
            throw new NotFoundException();
        }
        DishHibernate updatedDish = dishHibernate.get();
        updatedDish.setName(name);
        updatedDish.setDescription(description);
        updatedDish.setPrice(price);
        updatedDish.setAvailable(isAvailable);

        dishHibernateRepository.save(updatedDish);

        return dishHibernate.map(this::toDomain);
    }

    @Override
    public void removeDish(Long id) {
        dishHibernateRepository.deleteById(id);
    }

    public Dish toDomain(DishHibernate hibernate){
        return new Dish(
                hibernate.getId(),
                hibernate.getName(),
                hibernate.getDescription(),
                hibernate.getPrice(),
                hibernate.getAvailable()
        );
    }
}
