package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Client;
import pl.groupproject.restorantApp.domain.model.Employee;
import pl.groupproject.restorantApp.domain.model.OrderDish;
import pl.groupproject.restorantApp.domain.model.Receipt;
import pl.groupproject.restorantApp.domain.repository.OrderDishRepository;
import pl.groupproject.restorantApp.infrastructure.entity.OrderDishHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.OrderDishHibernateRepository;

@Repository
public class OrderDishRepositoryImpl implements OrderDishRepository {

    private final OrderDishHibernateRepository orderDishHibernateRepository;
    private final DishRepositoryImpl dishRepository;

    @Autowired
    public OrderDishRepositoryImpl(OrderDishHibernateRepository orderDishHibernateRepository,
                                   DishRepositoryImpl dishRepository
    ) {
        this.orderDishHibernateRepository = orderDishHibernateRepository;
        this.dishRepository = dishRepository;
    }

    @Override
    public Iterable<OrderDishHibernate> getAllOrderDish() {
        return orderDishHibernateRepository.findAll();
    }

    public OrderDish toDomain(OrderDishHibernate hibernate){
        return new OrderDish(
                hibernate.getId(),
                hibernate.getQuantity(),
                dishRepository.toDomain(hibernate.getDish()),
                new Receipt(
                        hibernate.getReceipt().getId(),
                        hibernate.getReceipt().getPriceWithDiscount(),
                        hibernate.getReceipt().getTip(),
                        hibernate.getReceipt().getReceiptDate(),
                        new Client(
                                hibernate.getReceipt().getClient().getId(),
                                hibernate.getReceipt().getClient().getName(),
                                hibernate.getReceipt().getClient().getSurname(),
                                hibernate.getReceipt().getClient().getDiscount()),
                        new Employee(hibernate.getReceipt().getEmployee().getId(),
                                hibernate.getReceipt().getEmployee().getPesel(),
                                hibernate.getReceipt().getEmployee().getName(),
                                hibernate.getReceipt().getEmployee().getSurname(),
                                hibernate.getReceipt().getEmployee().getPosition(),
                                hibernate.getReceipt().getEmployee().getSalary(),
                                new Employee(hibernate.getReceipt().getEmployee().getBoss().getId(),
                                        hibernate.getReceipt().getEmployee().getBoss().getPesel(),
                                        hibernate.getReceipt().getEmployee().getBoss().getName(),
                                        hibernate.getReceipt().getEmployee().getBoss().getSurname(),
                                        hibernate.getReceipt().getEmployee().getBoss().getPosition(),
                                        hibernate.getReceipt().getEmployee().getBoss().getSalary()))
                )
        );
    }
}
