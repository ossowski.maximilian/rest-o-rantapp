package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Client;
import pl.groupproject.restorantApp.domain.model.Employee;
import pl.groupproject.restorantApp.domain.model.OrderDrink;
import pl.groupproject.restorantApp.domain.model.Receipt;
import pl.groupproject.restorantApp.domain.repository.OrderDrinkRepository;
import pl.groupproject.restorantApp.infrastructure.entity.OrderDrinkHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.DrinkHibernateRepository;
import pl.groupproject.restorantApp.infrastructure.repository.OrderDrinkHibernateRepository;

@Repository
public class OrderDrinkRepositoryImpl implements OrderDrinkRepository {

    private final OrderDrinkHibernateRepository orderDrinkHibernateRepository;
    private final DrinkHibernateRepository drinkHibernateRepository;
    private final DrinkRepositoryImpl drinkRepository;

    @Autowired
    public OrderDrinkRepositoryImpl(OrderDrinkHibernateRepository orderDrinkHibernateRepository,
                                    DrinkHibernateRepository drinkHibernateRepository,
                                    DrinkRepositoryImpl drinkRepository) {
        this.orderDrinkHibernateRepository = orderDrinkHibernateRepository;
        this.drinkHibernateRepository = drinkHibernateRepository;
        this.drinkRepository = drinkRepository;
    }

    @Override
    public Iterable<OrderDrinkHibernate> getAllOrderDrink() {
        return orderDrinkHibernateRepository.findAll();
    }

    public OrderDrink toDomain(OrderDrinkHibernate orderDrinkHibernate) {
        return new OrderDrink(
                orderDrinkHibernate.getId(),
                orderDrinkHibernate.getQuantity(),
                drinkRepository.toDomain(orderDrinkHibernate.getDrink()),
                        new Receipt(
                                orderDrinkHibernate.getReceipt().getId(),
                                orderDrinkHibernate.getReceipt().getPriceWithDiscount(),
                                orderDrinkHibernate.getReceipt().getTip(),
                                orderDrinkHibernate.getReceipt().getReceiptDate(),
                                new Client(
                                        orderDrinkHibernate.getReceipt().getClient().getId(),
                                        orderDrinkHibernate.getReceipt().getClient().getName(),
                                        orderDrinkHibernate.getReceipt().getClient().getSurname(),
                                        orderDrinkHibernate.getReceipt().getClient().getDiscount()),
                                new Employee(orderDrinkHibernate.getReceipt().getEmployee().getId(),
                                        orderDrinkHibernate.getReceipt().getEmployee().getPesel(),
                                        orderDrinkHibernate.getReceipt().getEmployee().getName(),
                                        orderDrinkHibernate.getReceipt().getEmployee().getSurname(),
                                        orderDrinkHibernate.getReceipt().getEmployee().getPosition(),
                                        orderDrinkHibernate.getReceipt().getEmployee().getSalary(),
                                        new Employee(orderDrinkHibernate.getReceipt().getEmployee().getBoss().getId(),
                                                orderDrinkHibernate.getReceipt().getEmployee().getBoss().getPesel(),
                                                orderDrinkHibernate.getReceipt().getEmployee().getBoss().getName(),
                                                orderDrinkHibernate.getReceipt().getEmployee().getBoss().getSurname(),
                                                orderDrinkHibernate.getReceipt().getEmployee().getBoss().getPosition(),
                                                orderDrinkHibernate.getReceipt().getEmployee().getBoss().getSalary()))
                        )
        );
    }

}
