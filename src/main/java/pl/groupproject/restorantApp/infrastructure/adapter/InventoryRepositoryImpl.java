package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Inventory;
import pl.groupproject.restorantApp.domain.repository.InventoryRepository;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.infrastructure.entity.InventoryHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.InventoryHibernateRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InventoryRepositoryImpl implements InventoryRepository {

    private final InventoryHibernateRepository inventoryHibernateRepository;

    @Autowired
    public InventoryRepositoryImpl(InventoryHibernateRepository inventoryHibernateRepository) {
        this.inventoryHibernateRepository = inventoryHibernateRepository;
    }

    @Override
    public Inventory createInventory(
            String name,
            BigDecimal quantity) {

        InventoryHibernate inventoryHibernate = new InventoryHibernate(
                null,
                name,
                quantity
        );

        inventoryHibernateRepository.save(inventoryHibernate);
        return toDomain(inventoryHibernate);

    }

    @Override
    public Inventory updateInventory(
            Long id,
            String name,
            BigDecimal quantity) {

        Optional<InventoryHibernate> optionalInventoryHibernate = inventoryHibernateRepository.findById(id);
        if (!optionalInventoryHibernate.isPresent()) {
            throw new NotFoundException();
        }

        InventoryHibernate inventoryToUpdate = optionalInventoryHibernate.get();

        inventoryToUpdate.setName(name);
        inventoryToUpdate.setQuantity(quantity);

        inventoryHibernateRepository.save(inventoryToUpdate);

        return toDomain(inventoryToUpdate);
    }

    @Override
    public Optional<Inventory> getInventory(Long id) {
        return inventoryHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Inventory> getAllInventory(Pageable pageable) {
        Page<InventoryHibernate> page = inventoryHibernateRepository.findAll(pageable);
        List<InventoryHibernate> inventoryHibernates = page.getContent();
        return inventoryHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeInventory(Long id) {
        inventoryHibernateRepository.deleteById(id);
    }

    public Inventory toDomain(InventoryHibernate inventoryHibernate) {
        return new Inventory(
                inventoryHibernate.getId(),
                inventoryHibernate.getName(),
                inventoryHibernate.getQuantity()
        );
    }

}
