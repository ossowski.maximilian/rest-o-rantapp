package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Drink;
import pl.groupproject.restorantApp.domain.repository.DrinkRepository;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.infrastructure.entity.DrinkHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.DrinkHibernateRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DrinkRepositoryImpl implements DrinkRepository {

    private final DrinkHibernateRepository drinkHibernateRepository;

    @Autowired
    public DrinkRepositoryImpl(DrinkHibernateRepository drinkHibernateRepository) {
        this.drinkHibernateRepository = drinkHibernateRepository;
    }

    @Override
    public Drink createDrink(String name,
                             String description,
                             BigDecimal price,
                             Integer volumeMl,
                             Boolean isAvailable) {

        DrinkHibernate drinkHibernate = new DrinkHibernate(
                null,
                name,
                description,
                price,
                volumeMl,
                isAvailable
        );

        drinkHibernateRepository.save(drinkHibernate);
        return toDomain(drinkHibernate);
    }

    @Override
    public Optional<Drink> getDrink(Long id) {
        return drinkHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Drink> getAllDrink(Pageable pageable) {
        return drinkHibernateRepository.findAll(pageable)
                .getContent()
                .stream()
                .map(this::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Drink> updateDrink(Long id,
                                       String name,
                                       String description,
                                       BigDecimal price,
                                       Integer volumeMl,
                                       Boolean isAvailable) {
        Optional<DrinkHibernate> drinkHibernate = drinkHibernateRepository.findById(id);
        if (!drinkHibernate.isPresent()) {
            throw new NotFoundException();
        }
        DrinkHibernate updatedDrink = drinkHibernate.get();
        updatedDrink.setName(name);
        updatedDrink.setDescription(description);
        updatedDrink.setPrice(price);
        updatedDrink.setVolumeMl(volumeMl);
        updatedDrink.setAvailable(isAvailable);

        drinkHibernateRepository.save(updatedDrink);
        return drinkHibernate.map(this::toDomain);

    }

    @Override
    public void removeDrink(Long id) {
        drinkHibernateRepository.deleteById(id);
    }

    public Drink toDomain(DrinkHibernate hibernate) {
        return new Drink(
                hibernate.getId(),
                hibernate.getName(),
                hibernate.getDescription(),
                hibernate.getPrice(),
                hibernate.getVolumeMl(),
                hibernate.getAvailable()
        );
    }
}
