package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Client;
import pl.groupproject.restorantApp.domain.repository.ClientRepository;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.infrastructure.entity.ClientHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.ClientHibernateRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    private final ClientHibernateRepository clientHibernateRepository;

    @Autowired
    public ClientRepositoryImpl(ClientHibernateRepository clientHibernateRepository) {
        this.clientHibernateRepository = clientHibernateRepository;
    }

    @Override
    public Client createClient(
            String name,
            String surname,
            BigDecimal discount) {

        ClientHibernate clientHibernate = new ClientHibernate(
                null,
                name,
                surname,
                discount
        );

        clientHibernateRepository.save(clientHibernate);
        return toDomain(clientHibernate);
    }

    @Override
    public Client updateClient(
            Long id,
            String name,
            String surname,
            BigDecimal discount) {

        Optional<ClientHibernate> optionalClientHibernate = clientHibernateRepository.findById(id);
        if (!optionalClientHibernate.isPresent()) {
            throw new NotFoundException();
        }

        ClientHibernate clientToUpdate = optionalClientHibernate.get();

        clientToUpdate.setName(name);
        clientToUpdate.setSurname(surname);
        clientToUpdate.setDiscount(discount);

        clientHibernateRepository.save(clientToUpdate);

        return toDomain(clientToUpdate);
    }

    @Override
    public Optional<Client> getClient(Long id) {
        return clientHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Client> getAllClients(Pageable pageable) {
        Page<ClientHibernate> page = clientHibernateRepository.findAll(pageable);
        List<ClientHibernate> clientHibernates = page.getContent();
        return clientHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeClient(Long id) {
        clientHibernateRepository.deleteById(id);
    }

    Client toDomain(ClientHibernate clientHibernate) {
        return new Client(
                clientHibernate.getId(),
                clientHibernate.getName(),
                clientHibernate.getSurname(),
                clientHibernate.getDiscount()
        );
    }

}
