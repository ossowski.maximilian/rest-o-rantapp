package pl.groupproject.restorantApp.infrastructure.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import pl.groupproject.restorantApp.domain.model.Employee;
import pl.groupproject.restorantApp.domain.repository.EmployeeRepository;
import pl.groupproject.restorantApp.exception.NotFoundException;
import pl.groupproject.restorantApp.infrastructure.entity.EmployeeHibernate;
import pl.groupproject.restorantApp.infrastructure.repository.EmployeeHibernateRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final EmployeeHibernateRepository employeeHibernateRepository;


    @Autowired
    public EmployeeRepositoryImpl(EmployeeHibernateRepository employeeHibernateRepository) {
        this.employeeHibernateRepository = employeeHibernateRepository;
    }

    @Override
    public Employee createEmployee(
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Employee boss) {

        EmployeeHibernate bossHibernate = new EmployeeHibernate(
                boss.getId(),
                boss.getPesel(),
                boss.getName(),
                boss.getSurname(),
                boss.getPosition(),
                boss.getSalary()
        );

        EmployeeHibernate employeeHibernate = new EmployeeHibernate(
                null,
                pesel,
                name,
                surname,
                position,
                salary,
                bossHibernate
        );

        employeeHibernateRepository.save(employeeHibernate);

        return toDomain(employeeHibernate);

    }

    @Override
    public Employee updateEmployee(
            Long id,
            Long pesel,
            String name,
            String surname,
            String position,
            BigDecimal salary,
            Long bossId) {

        Optional<EmployeeHibernate> optionalEmployeeHibernate = employeeHibernateRepository.findById(id);
        if (!optionalEmployeeHibernate.isPresent()) {
            throw new NotFoundException();
        }

        Optional<EmployeeHibernate> optionalBossHibernate = employeeHibernateRepository.findById(bossId);
        if (!optionalBossHibernate.isPresent()) {
            throw new NotFoundException();
        }

        EmployeeHibernate employeeToUpdate = optionalEmployeeHibernate.get();

        employeeToUpdate.setPesel(pesel);
        employeeToUpdate.setName(name);
        employeeToUpdate.setSurname(surname);
        employeeToUpdate.setPosition(position);
        employeeToUpdate.setSalary(salary);
        employeeToUpdate.setBoss(optionalBossHibernate.get());

        employeeHibernateRepository.save(employeeToUpdate);

        return toDomain(employeeToUpdate);
    }

    @Override
    public Optional<Employee> getEmployee(Long id) {
        return employeeHibernateRepository.findById(id).map(this::toDomainWithBoss);
    }

    @Override
    public Optional<Employee> getEmployeeWithBoss(Long id) {
        return employeeHibernateRepository.findById(id).map(this::toDomainWithBoss);
    }

    @Override
    public List<Employee> getAllEmployees(Pageable pageable) {
        Page<EmployeeHibernate> page = employeeHibernateRepository.findAll(pageable);
        List<EmployeeHibernate> employeeHibernates = page.getContent();
        return employeeHibernates.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeEmployee(Long id) {
        employeeHibernateRepository.deleteById(id);
    }

    public Employee toDomain(EmployeeHibernate employeeHibernate) {
        return new Employee(
                employeeHibernate.getId(),
                employeeHibernate.getPesel(),
                employeeHibernate.getName(),
                employeeHibernate.getSurname(),
                employeeHibernate.getPosition(),
                employeeHibernate.getSalary()
        );
    }

    public Employee toDomainWithBoss(EmployeeHibernate employeeHibernate) {
        return new Employee(
                employeeHibernate.getId(),
                employeeHibernate.getPesel(),
                employeeHibernate.getName(),
                employeeHibernate.getSurname(),
                employeeHibernate.getPosition(),
                employeeHibernate.getSalary(),
                this.toDomain(employeeHibernate.getBoss())
        );
    }

}
