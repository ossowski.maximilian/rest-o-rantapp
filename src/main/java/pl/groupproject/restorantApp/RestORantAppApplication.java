package pl.groupproject.restorantApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
		"pl.groupproject.restorantApp.infrastructure",
		"pl.groupproject.restorantApp.controller",
		"pl.groupproject.restorantApp.domain"
		})
public class RestORantAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestORantAppApplication.class, args);
	}

}
